from django.shortcuts import render,redirect
from . import forms, models
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
@csrf_exempt

def indexStory6(request):
    if request.method == "POST":
        if 'nama_kegiatan' in request.POST:
            formKegiatan = forms.KegiatanCreateForm(request.POST)
            if formKegiatan.is_valid():
                context = models.Kegiatan()
                context.nama_kegiatan = formKegiatan.cleaned_data['nama_kegiatan']
                context.save()
        elif 'nama_peserta' in request.POST and 'id_kegiatan' in request.POST:
            formPeserta = forms.PesertaCreateForm(request.POST)
            if formPeserta.is_valid():
                models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                context = models.Peserta()
                context.nama_peserta = formPeserta.cleaned_data['nama_peserta']
                context.kegiatan = models.Kegiatan.objects.get(id=request.POST['id_kegiatan'])
                context.save()

    lst_kegiatan = models.Kegiatan.objects.all()
    data_lengkap = []
    for kegiatan in lst_kegiatan:
        peserta = models.Peserta.objects.filter(kegiatan = kegiatan)
        lst_peserta = []
        for i in peserta:
            lst_peserta.append(i)
        data_lengkap.append((kegiatan, lst_peserta))
    
    context_dict = {
        'form_kegiatan' : forms.KegiatanCreateForm,
        'form_peserta' : forms.PesertaCreateForm,
        'kegiatan'  : data_lengkap
    }

    return render(request, 'jadwal.html', context_dict)
    