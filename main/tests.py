from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .views import indexStory6
from .models import Kegiatan
from .models import Peserta
from .models import *
from .forms import *

class Story6UnitTest(TestCase):
    def test_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200) 

    def test_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan = "apex")
        counter = Kegiatan.objects.all().count()
        self.assertEqual(counter, 1)
    
    def test_kegiatan_peserta(self):
        Kegiatan.objects.create(nama_kegiatan = "apex")
        Peserta.objects.create(nama_peserta = "apex")
        counter = Kegiatan.objects.all().count()
        counter += Peserta.objects.all().count()
        self.assertEqual(counter, 2)

    def test_peserta(self):
        Peserta.objects.create(nama_peserta = "apex")
        counter = Peserta.objects.all().count()
        self.assertEqual(counter, 1)
    
    def test_model_kegiatan(self):
        Kegiatan.objects.create(nama_kegiatan = "apex")
        kegiatan  = Kegiatan.objects.get(nama_kegiatan = "apex")
        self.assertEqual(str(kegiatan), "apex")

    def test_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'jadwal.html')

    def test_form_valid(self):
        data = {'nama_kegiatan':"ngoding"}
        kegiatan_form =  KegiatanCreateForm(data=data)
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"ngoding")
    
    def test_form_post(self):
        test_str = 'xyz'
        response_post = Client().post('', {'nama_kegiatan':"xyz"})
        self.assertEqual(response_post.status_code,200)
        kegiatan_form = KegiatanCreateForm(data={'nama_kegiatan':test_str})
        self.assertTrue(kegiatan_form.is_valid())
        self.assertEqual(kegiatan_form.cleaned_data['nama_kegiatan'],"xyz")




    



    
    
